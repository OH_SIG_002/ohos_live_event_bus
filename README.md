# LiveEventBus

## Introduction

> LiveEventBus is a message bus that has lifecycle awareness and supports Sticky, cross-process, and cross-application message sending.

 ![showeventbus](./show/showeventbus_EN.gif)


## How to Install
```shell
ohpm install @ohos/liveeventbus 

```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).


## How to Use

 Use the project in pages.

```js
// Import the project.
import { LiveEventBus,Lifecycle,MState } from '@ohos/liveeventbus'
import router from '@system.router';

const KEY_TEST_CLOSE_ALL_PAGE = "key_test_close_all_page";
@Entry
@Component
struct Demo {
 private mLifecycle: Lifecycle;
 aboutToAppear() {
  // Create a lifecycle awareness object.
  this.mLifecycle = new Lifecycle(MState.STARTED)
  // Subscribe to messages.
  LiveEventBus
      .get<boolean>(KEY_TEST_CLOSE_ALL_PAGE)
      .observe(this, {
        onChanged(b:boolean) {
          if (b) {
            router.clear()
            router.back()
          }
        }
      });
 }
 build() {
    Column({ space: 10 }) {
      Text('LiveEventBus Demo')
        .fontSize(30)
        .fontWeight(FontWeight.Bold)
      Scroll() {
        Column({ space: 5 }) {
         Button('Close All').onClick (event => {
            this.closeAll()
          })
        }
       }
     }
   }
 
 closeAll() {
    // Send a message.
    LiveEventBus.get(KEY_TEST_CLOSE_ALL_PAGE).post(true);
  }
    
 aboutToDisappear() {
    destroyService()
    this.mLifecycle.markState(MState.DESTROYED)
  }
 
 // Lifecycle awareness object
 getLifecycle(): Lifecycle{
    return this.mLifecycle
  }
}
```

### Subscribing to Messages

- Subscribe to messages in lifecycle awareness mode.

  ```js
      LiveEventBus
        .get<string>("name")
        .observe(this, {
          onChanged(s) {
            showToast(s);
          }
        });
  ```

- Subscribe to messages in forever mode.

  ```js
   LiveEventBus
      .get<string>("name")
      .observeForever(observer);
  ```

### Sending Messages

- Directly send a message without defining it.

  ```
  LiveEventBus
  	.get("some_key")
  	.post(some_value);
  ```

- Send a message to a subscriber registered with `observeForver`.

  ```
  LiveEventBus.get(KEY_TEST_OBSERVE_FOREVER)
        .post("Message To ForeverObserver: " + nextInt(100));
  ```

- Send a message in a process.

  ```
  post(value: T): void
  ```

- Send a message between applications.

  ```
  postAcrossProcess(value: T): void
  ```

- Send a message after a delay.

  ```
  postOrderly(value: T): void
  ```

- Send a message after a delay with lifecycle.

  ```
  postDelay(value: T, delay: number, sender?: LifecycleOwner): void
  ```

- Send a message in broadcast mode.

  ```
  broadcast(value: T): void
  ```

- Define a message and then send it.

  ```js
  class DemoEvent {
    content: string
  
    constructor(content: string) {
      this.content = content
    }
  }
  
  LiveEventBus
        .get<string>('DemoEvent')
        .post(JSON.stringify(new DemoEvent("Hello world")));
  ```

For details, see the implementation on the demo page of the open-source library.


## Available APIs
` import { LiveEventBus } from '@ohos/liveeventbus' `

1. Sends a message in a process.
   ` LiveEventBus.get("key").post() `
2. Sends a message between applications.
   ` LiveEventBus.get("key").postAcrossApp() `
3. Sends a message in a process after a delay, with a lifecycle.
   ` LiveEventBus.get("key").postDelay() `
4. Sends a message in broadcast mode.
   ` LiveEventBus.get("key").broadcast() `
5. Registers an observer to aware the lifecycle and automatically cancel the subscription.
   ` LiveEventBus.get("key").observe() `
6. Registers an observer. If a message is sent before, the observer can receive the message during its registration (message synchronization).
   ` LiveEventBus.get("key").observeSticky() `
7. Registers an observer. It must be manually unbound.
   ` LiveEventBus.get("key").observeForever() `
8. Registers an observer. If a message is sent before, the observer can receive the message during its registration (message synchronization).
   ` LiveEventBus.get("key").observeStickyForever() `
9. Removes the observer that is registered through `observeForever` or `observeStickyForever`.
   ` LiveEventBus.get("key").removeObserver() `
10. Unsubscribes from messages between applications.
      ` LiveEventBus.get("key").unsubscribe() `

# Constraints

This project has been verified in the following version:

DevEco Studio NEXT Developer Preview1: (4.1.3.501), SDK: API 11 (4.1.0.57 SP6)

## Directory Structure
````
|---- LiveEventBus  
|     |---- entry  # Sample code
|     |---- library  # LiveEventBus library
|           |---- Index.ts  # External APIs
|           └─src/main/ets/LiveEventBus
|                          ├─core # Core code, event bus processing class
|                          ├─lifecycle # Lifecycle data processing
|                          ├─logger # Print logs
|                          └─tslivedata # livedata in .ts
|                 |---- GlobalContext  # globalThis is replaced by GlobalContext.
|                 |---- LiveEventBus  # LiveEventBus core class
|---- README.md  # Readme            
|---- README_zh.md  # Readme        
````

## How to Contribute
If you find any problem during the use, submit an [issue](https://gitee.com/openharmony-sig/LiveEventBus/issues) or a [PR](https://gitee.com/openharmony-sig/LiveEventBus/pulls).

## License
This project is incensed under [Apache License 2.0](https://gitee.com/openharmony-sig/LiveEventBus/blob/master/LICENSE).
